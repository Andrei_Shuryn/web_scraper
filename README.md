# web_scraper

Scraper to collect data from car sales advertisements

## Getting started

python3 -m venv venv

source ./venv/bin/activate

pip install -r requirements.txt

python3 scraper.py

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/Andrei_Shuryn/web_scraper.git
git branch -M main
git push -uf origin main
```

# Scraper

Scraper to collect data from car sales advertisements

